import express from "express";
import bodyParser from "body-parser";
import { v4 as uuidv4 } from "uuid";

import CustomerService from "./services/CustomerService.mjs";
import CustomerRepository from "./repository/CustomerRepository.mjs";

import customerSchema from "./schema/customer.mjs";
import { schema } from "./schema/index.mjs";

const app = express();
app.use(bodyParser.json());

const customerService = new CustomerService(new CustomerRepository());

app.get("/customers/healthcheck", (req, res) => {
  res.send("Ok");
});

app.post("/customers", schema(customerSchema), async (req, res) => {
  try {
    const id = uuidv4();
    await customerService.save(id, req.body);

    res.json({
      id,
      self: `${process.env.BASE_URL}/customers/${id}`,
    });
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: error.message,
    });
  }
});

app.get("/customers", async (req, res) => {
  try {
    const customers = await customerService.list(req.query.LastEvaluatedKey);
    res.json(customers);
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: error.message,
    });
  }
});

app.get("/customers/:id", async (req, res) => {
  try {
    const customer = await customerService.get(req.params.id);
    res.json({ ...customer });
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: error.message,
    });
  }
});

app.delete("/customers/:id", async (req, res) => {
  try {
    await customerService.delete(req.params.id);
    res.status(200).send();
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: error.message,
    });
  }
});

app.put("/customers/:id", schema(customerSchema), async (req, res) => {
  try {
    await customerService.save(req.params.id, req.body);
    res.status(200).send();
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: error.message,
    });
  }
});

app.listen(process.env.PORT, () => {
  console.log(`App listening on port ${process.env.PORT}`);
});

export default app;
