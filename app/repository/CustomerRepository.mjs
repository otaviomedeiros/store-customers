import AWS from "aws-sdk";

const dynamodb = new AWS.DynamoDB({
  region: process.env.REGION,
  endpoint: process.env.DB_ENDPOINT,
});

class CustomerRepository {
  async list(LastEvaluatedKey = undefined) {
    const params = {
      TableName: "Customers",
      Limit: 5,
      ExclusiveStartKey: LastEvaluatedKey
        ? { Id: { S: LastEvaluatedKey } }
        : undefined,
    };

    const customers = await dynamodb.scan(params).promise();
    return customers;
  }

  async get(id) {
    const customer = dynamodb
      .getItem({
        TableName: "Customers",
        Key: {
          Id: { S: id },
        },
      })
      .promise();

    return customer;
  }

  async save(item) {
    return await dynamodb
      .putItem({
        TableName: "Customers",
        Item: item,
      })
      .promise();
  }

  async delete(id) {
    return await dynamodb
      .deleteItem({
        TableName: "Customers",
        Key: {
          Id: { S: id },
        },
      })
      .promise();
  }
}

export default CustomerRepository;
