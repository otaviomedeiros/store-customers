export default {
  type: "object",
  properties: {
    name: { type: "string" },
    email: { type: "string" },
    address: {
      type: "object",
      properties: {
        street: { type: "string" },
        number: { type: "number" },
      },
      required: ["street", "number"],
    },
  },
  required: ["name", "email", "address"],
};
