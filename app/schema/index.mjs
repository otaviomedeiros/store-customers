import JsonSchema from "jsonschema";
const validate = JsonSchema.validate;

export const schema = (jsonSchema) => {
  return (req, res, next) => {
    const validationResult = validate(req.body, jsonSchema);

    if (validationResult.valid) {
      next();
    } else {
      res.status(400).json({
        status: 400,
        messages: validationResult.errors.map((error) => error.message),
      });
    }
  };
};
