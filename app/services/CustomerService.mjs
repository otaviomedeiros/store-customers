class CustomerService {
  constructor(customerRepository) {
    this.customerRepository = customerRepository;
  }

  async list(LastEvaluatedKey = undefined) {
    const customers = await this.customerRepository.list(LastEvaluatedKey);
    const result = {
      values: customers.Items.map(this.transformCustomer),
    };

    return customers.LastEvaluatedKey
      ? { LastEvaluatedKey: customers.LastEvaluatedKey.Id.S, ...result }
      : result;
  }

  async get(id) {
    const customer = await this.customerRepository.get(id);
    return this.transformCustomer(customer.Item);
  }

  async save(id, customer) {
    const item = {
      Id: {
        S: id,
      },
      Email: {
        S: customer.email,
      },
      Name: {
        S: customer.name,
      },
      Address: {
        M: {
          Street: {
            S: customer.address.street,
          },
          Number: {
            N: customer.address.number.toString(),
          },
        },
      },
    };

    return this.customerRepository.save(item);
  }

  async delete(id) {
    return this.customerRepository.delete(id);
  }

  transformCustomer(item) {
    let customer = {
      id: item.Id.S,
      email: item.Email.S,
      name: item.Name.S,
    };

    item.Address &&
      (customer = {
        ...customer,
        address: {
          street: item.Address.M.Street.S,
          number: parseInt(item.Address.M.Number.N),
        },
      });

    return customer;
  }
}

export default CustomerService;
