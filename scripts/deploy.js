const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.DEFAULT_REGION });
const ecs = new AWS.ECS();

const createNewRevisionObject = (currentTaskDefinition) => ({
  containerDefinitions: currentTaskDefinition.containerDefinitions.map(
    (containerDefinition) => ({
      ...containerDefinition,
      image: `${process.env.IMAGE_NAME}:${process.env.BITBUCKET_COMMIT}`,
    })
  ),
  family: currentTaskDefinition.family,
  cpu: currentTaskDefinition.cpu,
  executionRoleArn: currentTaskDefinition.executionRoleArn,
  taskRoleArn: currentTaskDefinition.taskRoleArn,
  memory: currentTaskDefinition.memory,
  networkMode: currentTaskDefinition.networkMode,
  requiresCompatibilities: ["FARGATE"],
});

const createNewTaskDefinitionRevision = async () => {
  const { taskDefinition: currentTaskDefinition } = await ecs
    .describeTaskDefinition({
      taskDefinition: process.env.TASK_DEFINITION_NAME,
    })
    .promise();

  try {
    const newRevisionResult = await ecs
      .registerTaskDefinition(createNewRevisionObject(currentTaskDefinition))
      .promise();
    console.log("New revision created: ");
    console.log(JSON.stringify(newRevisionResult, null, 4));

    return newRevisionResult;
  } catch (e) {
    console.log(
      "Something went wrong when trying to create a new task definition revision: ",
      e
    );
  }
};

const updateEcsService = async (taskDefinition) => {
  try {
    const updateServiceResult = await ecs
      .updateService({
        cluster: process.env.CLUSTER_NAME,
        service: process.env.ECS_SERVICE_NAME,
        taskDefinition: taskDefinition.taskDefinitionArn,
        forceNewDeployment: true,
      })
      .promise();

    console.log("ECS service updated: ");
    console.log(JSON.stringify(updateServiceResult, null, 4));

    return updateServiceResult;
  } catch (e) {
    console.log(
      "Something went wrong when trying to update the ECS service: ",
      e
    );
  }
};

const deploy = async () => {
  const { taskDefinition } = await createNewTaskDefinitionRevision();
  await updateEcsService(taskDefinition);

  console.log("Done!");
};

deploy();
