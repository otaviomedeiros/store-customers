import AWS from "aws-sdk";
import customersTable from "./tableDefinitions/customers.mjs";

const dynamodb = new AWS.DynamoDB({
  region: process.env.REGION,
  endpoint: process.env.DB_ENDPOINT,
});

export const createCustomersTable = () => {
  return dynamodb.createTable(customersTable).promise();
};

export const deleteCustomersTable = () => {
  return dynamodb.deleteTable({ TableName: "Customers" }).promise();
};
