import AWS from "aws-sdk";

const dynamodb = new AWS.DynamoDB({
  region: process.env.REGION,
  endpoint: process.env.DB_ENDPOINT,
});

import chai from "chai";
import chaiHttp from "chai-http";
chai.use(chaiHttp);
import app from "../../app/index.mjs";
const expect = chai.expect;

import {
  createCustomersTable,
  deleteCustomersTable,
} from "../../scripts/dynamodb/scripts.mjs";

import customerItems from "../data/customerItems.json";
import customersBody from "../data/customersBody.json";
import customerBody from "../data/customerBody.json";

describe("Customers API endpoints", () => {
  beforeEach(async () => {
    await createCustomersTable();
    const batch = {
      RequestItems: {
        Customers: customerItems.Items.map((item) => ({
          PutRequest: { Item: item },
        })),
      },
    };

    await dynamodb.batchWriteItem(batch).promise();
  });

  afterEach(async () => {
    await deleteCustomersTable();
  });

  describe("GET /customers", () => {
    it("should return a list of customers", (done) => {
      chai
        .request(app)
        .get("/customers")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.deep.equal(customersBody);
          done();
        });
    });
  });

  describe("GET /customers/:id", () => {
    it("should return the customer for the given id", (done) => {
      chai
        .request(app)
        .get("/customers/b5ef5bba-fa5e-40f3-98d4-d0a23234ad69")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.deep.equal(customerBody);
          done();
        });
    });
  });

  describe("DELETE /customers/:id", () => {
    it("should delete the customer for the given id", (done) => {
      chai
        .request(app)
        .get("/customers/b5ef5bba-fa5e-40f3-98d4-d0a23234ad69")
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("PUT /customers/:id", () => {
    it("should update the customer for the given id", (done) => {
      chai
        .request(app)
        .put("/customers/b5ef5bba-fa5e-40f3-98d4-d0a23234ad69")
        .send({
          name: "Some user",
          email: "new.email@gmail.com",
          address: {
            street: "rua das orquideas",
            number: 127,
          },
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("POST /customers", () => {
    it("should a new customer", (done) => {
      chai
        .request(app)
        .post("/customers")
        .send({
          name: "New user",
          email: "new.user@gmail.com",
          address: {
            street: "rua das orquideas",
            number: 127,
          },
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.id).to.not.empty;
          expect(res.body.self).to.contain("http://localhost:3000/customers/");
          done();
        });
    });
  });
});
