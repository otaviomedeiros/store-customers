import chai from "chai";
import chaiHttp from "chai-http";
chai.use(chaiHttp);
import app from "../../app/index.mjs";
const expect = chai.expect;

describe("Health Check", () => {
  it("should respond with 200", (done) => {
    chai
      .request(app)
      .get("/customers/healthcheck")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});
