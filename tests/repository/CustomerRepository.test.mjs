import AWS from "aws-sdk";

const dynamodb = new AWS.DynamoDB({
  region: process.env.REGION,
  endpoint: process.env.DB_ENDPOINT,
});

import chai from "chai";
const expect = chai.expect;
import {
  createCustomersTable,
  deleteCustomersTable,
} from "../../scripts/dynamodb/scripts.mjs";

import CustomerRepository from "../../app/repository/CustomerRepository.mjs";

import customerItems from "../data/customerItems.json";
import customerItem from "../data/customerItem.json";

describe("CustomerRepository", () => {
  const customerRepository = new CustomerRepository();
  beforeEach(async () => {
    await createCustomersTable();
  });

  afterEach(async () => {
    await deleteCustomersTable();
  });

  describe("#list()", () => {
    beforeEach(async () => {
      const batch = {
        RequestItems: {
          Customers: customerItems.Items.map((item) => ({
            PutRequest: { Item: item },
          })),
        },
      };

      await dynamodb.batchWriteItem(batch).promise();
    });

    it("should list 5 customers by default", async () => {
      const customers = await customerRepository.list();
      expect(customers).to.deep.equal(customerItems);
    });
  });

  describe("#get()", () => {
    beforeEach(async () => {
      await dynamodb
        .putItem({ ...customerItem, TableName: "Customers" })
        .promise();
    });

    it("should retrieve a single customer", async () => {
      const customer = await customerRepository.get(
        "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69"
      );

      expect(customer).to.deep.equal(customerItem);
    });
  });

  describe("#save(customer)", () => {
    it("should save a customer", async () => {
      await customerRepository.save(customerItem.Item);

      const savedCustomer = await dynamodb
        .getItem({
          TableName: "Customers",
          Key: { Id: { S: "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69" } },
        })
        .promise();

      expect(savedCustomer).to.deep.equal(customerItem);
    });
  });

  describe("#delete(id)", () => {
    beforeEach(async () => {
      await dynamodb
        .putItem({ ...customerItem, TableName: "Customers" })
        .promise();
    });

    it("should delete a customer", async () => {
      await customerRepository.delete("b5ef5bba-fa5e-40f3-98d4-d0a23234ad69");

      const savedCustomer = await dynamodb
        .getItem({
          TableName: "Customers",
          Key: { Id: { S: "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69" } },
        })
        .promise();

      expect(savedCustomer).to.deep.equal({});
    });
  });
});
