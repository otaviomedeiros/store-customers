import chai from "chai";
import sinonChai from "sinon-chai";
const expect = chai.expect;
import sinon from "sinon";
chai.use(sinonChai);

import { schema } from "../../app/schema/index.mjs";
import customerSchema from "../../app/schema/customer.mjs";
import customerBody from "../data/customerBody.json";

describe("Json Schema Validation", () => {
  it("should call next() when json schema is valid", () => {
    const req = { body: customerBody };
    const res = {};
    const next = () => {};

    const spy = sinon.spy(next);

    const validateSchema = schema(customerSchema);
    validateSchema(req, res, spy);

    expect(spy).to.have.been.calledOnce;
  });

  it("should set status 400 when json schema is invalid", () => {
    const req = { body: { name: "User Name" } };
    const responseObject = { json() {} };
    const res = {
      status() {
        return responseObject;
      },
    };
    const next = () => {};

    const spy = sinon.spy(next);
    const spyRes = sinon.spy(res, "status");

    const validateSchema = schema(customerSchema);
    validateSchema(req, res, spy);

    expect(spy).to.have.not.been.called;
    expect(spyRes).to.have.been.calledWith(400);
  });
});
