import chai from "chai";
import sinon from "sinon";
const expect = chai.expect;

import CustomerService from "../../app/services/CustomerService.mjs";
import CustomerRepository from "../../app/repository/CustomerRepository.mjs";

import customerItems from "../data/customerItems.json";
import customersBody from "../data/customersBody.json";
import customerItem from "../data/customerItem.json";
import customerBody from "../data/customerBody.json";

describe("Customer service", () => {
  describe("#list()", () => {
    it("should return all customers", async () => {
      const customerRepository = new CustomerRepository();
      const mock = sinon.mock(customerRepository);
      const customerService = new CustomerService(customerRepository);

      mock.expects("list").returns(customerItems);

      const customers = await customerService.list();
      expect(customers).to.deep.equal(customersBody);
      mock.verify();
    });
  });

  describe("#get(id)", () => {
    it("should return the customer for the given id", async () => {
      const customerRepository = new CustomerRepository();
      const mock = sinon.mock(customerRepository);
      const customerService = new CustomerService(customerRepository);

      const id = "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69";

      mock.expects("get").withArgs(id).returns(customerItem);

      const customers = await customerService.get(id);
      expect(customerBody).to.deep.equal(customers);
      mock.verify();
    });
  });

  describe("#save(id, customer)", () => {
    it("should save the customer with the given id", async () => {
      const customerRepository = new CustomerRepository();
      const mock = sinon.mock(customerRepository);
      const customerService = new CustomerService(customerRepository);

      mock.expects("save").withArgs(customerItem.Item);

      await customerService.save(
        "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69",
        customerBody
      );
      mock.verify();
    });
  });

  describe("#delete(id)", () => {
    it("should delete the customer for the given id", async () => {
      const customerRepository = new CustomerRepository();
      const mock = sinon.mock(customerRepository);
      const customerService = new CustomerService(customerRepository);

      const id = "b5ef5bba-fa5e-40f3-98d4-d0a23234ad69";

      mock.expects("delete").withArgs(id);
      await customerService.delete(id, customerBody);
      mock.verify();
    });
  });
});
